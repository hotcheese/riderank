require 'test_helper'

class StatsControllerTest < ActionController::TestCase

  setup do
    # disable rides fixtures
    rides(:one).destroy
    rides(:two).destroy
  end


  def create_ride(price, distance, date, taxi=taxis(:taxiOne))
    Ride.create({start_address: 'Słomińskiego 7, Warszawa, Polska',
                 destination_address: 'Słomińskiego 7, Warszawa, Polska',
                 distance: distance,
                 price: price,
                 taxi: taxi,
                 date: date})
  end


  test 'should get show' do
    get :show
    assert_response :success
  end

  test 'should count week_distance and week_price' do
    create_ride(10, 20, Date.new(year=2016, month=3, mday=9))
    create_ride(5, 13.5, Date.new(year=2016, month=3, mday=8))
    @controller.instance_eval { set_week_stats(Date.new(year=2016, month=3, mday=9)) }

    assert_equal @controller.instance_eval { @week_price }, 15
    assert_equal @controller.instance_eval { @week_distance }, 33.5
  end

  test 'should count month_stats' do
    date0 = Date.new(year=2016, month=2, mday=15)
    date1 = Date.new(year=2016, month=3, mday=9)
    date2 = Date.new(year=2016, month=3, mday=8)

    create_ride(33, 44, date0, taxis(:taxiOne))
    create_ride(10, 20, date1, taxis(:taxiOne))
    create_ride(5, 13.5, date2, taxis(:taxiOne))
    create_ride(4, 7.5, date2, taxis(:taxiTwo))
    @controller.instance_eval { set_month_stats(Date.new(year=2016, month=3, mday=9)) }

    month_stats = @controller.instance_eval { @month_stats }

    assert_not month_stats[date0]

    assert_equal month_stats[date1][:distance_sum], 20.0
    assert_equal month_stats[date2][:distance_sum], 13.5+7.5

    assert_equal month_stats[date1][:avg_distance], 20.0
    assert_equal month_stats[date2][:avg_distance], (13.5+7.5)/2.0

    assert_equal month_stats[date1][:avg_price].to_f, 10.0
    assert_equal month_stats[date2][:avg_price].to_f, (5.0+4.0)/2.0

    assert_equal month_stats[date1][:taxis], 'NameOne'
    assert_equal month_stats[date2][:taxis], 'NameOne, NameTwo' #alphabetical order

  end

end
