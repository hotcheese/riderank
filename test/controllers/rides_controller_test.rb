require 'test_helper'

class RidesControllerTest < ActionController::TestCase
  setup do
    @ride = rides(:one)
    @ride.start_address = 'Marszałkowska 17, Warszawa, Polska'
    @ride.destination_address = 'Słomińskiego 7, Warszawa, Polska'
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rides)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ride" do
    assert_difference('Ride.count') do
      post :create, ride: {date: Date.today, destination_address: @ride.destination_address, start_address: @ride.start_address, distance: @ride.distance, price: @ride.price, taxi_id: @ride.taxi.id}
    end

    assert_redirected_to new_ride_path
  end

  test "should show ride" do
    get :show, id: @ride
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ride
    assert_response :success
  end

  test "should update ride" do
    patch :update, id: @ride, ride: {date: @ride.date, destination_address: @ride.destination_address, distance: @ride.distance, price: @ride.price, start_address: @ride.start_address}
    assert_response :success
  end

  test "should destroy ride" do
    assert_difference('Ride.count', -1) do
      delete :destroy, id: @ride
    end

    assert_redirected_to rides_path
  end
end
