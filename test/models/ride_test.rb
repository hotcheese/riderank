require 'test_helper'

class RideTest < ActiveSupport::TestCase

  def valid_ride_hash
    {start_address: 'Słomińskiego 7, Warszawa, Polska',
     destination_address: 'Słomińskiego 7, Warszawa, Polska',
     distance: 11,
     price: 12,
     taxi: taxis(:taxiOne),
     date: Date.today}
  end


  test 'should validate ride' do
    assert Ride.new(valid_ride_hash).valid?
  end

  test 'should not save ride without price' do
    ride_hash = valid_ride_hash
    ride_hash.delete(:price)

    ride = Ride.new(ride_hash)
    assert !ride.save, 'Saved the ride without a price'
  end

  test 'should not save ride without taxi' do
    ride_hash = valid_ride_hash
    ride_hash.delete(:taxi)

    ride = Ride.new(ride_hash)
    assert !ride.save, 'Saved the ride without a taxi'
  end

  test 'should validate distance is a number' do
    ride_hash = valid_ride_hash
    ride_hash[:distance] = 'abc'

    ride = Ride.new(ride_hash)
    assert_not ride.valid?
  end

  test 'should validate distance is a positive number' do
    ride_hash = valid_ride_hash
    ride_hash[:distance] = -10

    ride = Ride.new(ride_hash)
    assert_not ride.valid?
  end

  test 'should validate start_address' do
    ride_hash = valid_ride_hash
    ride_hash[:start_address] = 'Słomińskiego 7, Warszawa, Polska'
    ride = Ride.new(ride_hash)
    assert ride.valid?

    ride_hash = valid_ride_hash
    ride_hash[:start_address] = 'Słomińskiego 7, Warszawa'
    ride = Ride.new(ride_hash)
    assert_not ride.valid?

    ride_hash = valid_ride_hash
    ride_hash[:start_address] = 'Słomińskiego 7, Warszawa7'
    ride = Ride.new(ride_hash)
    assert_not ride.valid?

    ride_hash = valid_ride_hash
    ride_hash[:start_address] = 'Słomińskiego 7d/8, Wrocław, Polska'
    ride = Ride.new(ride_hash)
    assert ride.valid?

    ride_hash = valid_ride_hash
    ride_hash[:start_address] = 'Ulica 6, Miasto, Kraj, Abcd'
    ride = Ride.new(ride_hash)
    assert_not ride.valid?
  end

end
