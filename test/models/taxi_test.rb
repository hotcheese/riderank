require 'test_helper'

class TaxiTest < ActiveSupport::TestCase
  test 'the truth' do
    assert true
  end

  test 'should not save taxi without name' do
    taxi = Taxi.new(name: nil)
    assert !taxi.save, 'Saved the taxi without a name'
  end
end
