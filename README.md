# Riderank #
[![codebeat badge](https://codebeat.co/badges/7b67d6d9-891a-4e72-b2a4-b4d14b9c10e5)](https://codebeat.co/projects/bitbucket-org-hotcheese-riderank)

### Co to jest Riderank? ###

Riderank to aplikacja służąca do kontroli wydatków na taksówki. 

### Co potrafi aplikacja? ###
* Użytkownik może dodawać nowe przejazdy i taksówki.
* Odległość pomiędzy podanymi adresami jest automatycznie obliczana za pomocą Google Maps API (po stronie klienta).
* Użytkownik może wyświetlić informacje o przejechanym dystansie i wydanej kwocie w obecnym tygodniu.
* Użytkownik może wyświetlić statystyki z obecnego miesiąca dla poszczególnych dni.

### Gdzie można ją zobaczyć? ###

Aplikacja działa obecnie na Heroku: [Riderank](http://riderank.herokuapp.com)