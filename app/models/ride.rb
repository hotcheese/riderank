class Ride < ActiveRecord::Base

  address_regex = /\A[\p{L}0-9 ]+[\p{L}0-9\/]+, ?[\p{L}Z ]+, ?[\p{L} ]+\z/

  attr_accessor :start_address, :destination_address

  belongs_to :taxi

  validates_presence_of :date, :taxi
  validates :distance, :price, :numericality => {:greater_than_or_equal_to => 0}
  validates :start_address, :destination_address, format: {with: address_regex, message: 'is not valid'}

  public
  def taxi_name
    taxi ? taxi.name : '-'
  end


  def formatted_date
    ApplicationController.helpers.formatted_date(date)
  end


  def self.fetch_rides(today)
    Ride.where(date: today.beginning_of_month..today)
        .order(:date)
        .includes(:taxi)
        .all
        .group_by { |ride| ride.date }
  end

end


