// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function () {

    var on_address_changed = function () {
        var start = $('#ride_start_address').val();
        var destination = $('#ride_destination_address').val();

        if (start && destination) {

            var distanceService = new google.maps.DistanceMatrixService();
            distanceService.getDistanceMatrix({
                    origins: [start],
                    destinations: [destination],
                    travelMode: google.maps.TravelMode.DRIVING,
                    unitSystem: google.maps.UnitSystem.METRIC,
                    durationInTraffic: true,
                    avoidHighways: false,
                    avoidTolls: false
                },
                function (response, status) {
                    if (status == google.maps.DistanceMatrixStatus.OK) {
                        var distance_obj = response.rows[0].elements[0].distance;
                        if (distance_obj) {
                            var distance_km = distance_obj.value / 1000;
                        } else {
                            distance_km = '';
                        }
                        $('#ride_distance').val(distance_km);
                    } else {
                        $('#ride_distance').val('');
                        console.log('Error:', status);
                    }
                });
        }
    };

    $('#ride_start_address').change(on_address_changed);
    $('#ride_destination_address').change(on_address_changed);
});