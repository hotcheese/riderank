json.array!(@rides) do |ride|
  json.extract! ride, :id, :start_address, :destination_address, :price, :distance, :date
  json.url ride_url(ride, format: :json)
end
