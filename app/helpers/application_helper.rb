module ApplicationHelper
  def formatted_date(date)
    if date
      date.strftime("%B, #{date.day.ordinalize}")
    else
      '-'
    end
  end
end
