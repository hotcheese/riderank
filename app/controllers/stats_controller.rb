require 'set'
class StatsController < ApplicationController
  include ApplicationHelper

  def show
    today = Date.today
    set_week_stats(today)
    set_month_stats(today)
  end


  def set_week_stats(today)
    @week_distance = 0
    @week_price = 0

    Ride.where(:date => today.beginning_of_week..today).all.each do |ride|
      @week_distance += ride.distance
      @week_price += ride.price
    end
  end


  def set_month_stats(today)
    @month_stats = {}
    Ride.fetch_rides(today)
        .each do |day, rides|

      distance_sum = 0
      price_sum = 0
      taxis = Set.new

      rides.each do |ride|
        distance_sum += ride.distance
        price_sum += ride.price
        taxis.add(ride.taxi.name) if ride.taxi
      end

      @month_stats[day] = {
          day: formatted_date(day),
          distance_sum: distance_sum,
          avg_distance: distance_sum/rides.size,
          avg_price: price_sum/rides.size,
          taxis: join_names(taxis)}
    end
  end


  def join_names(taxis)
    taxis.to_a.sort.join(', ')
  end

end
