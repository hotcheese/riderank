class CreateRides < ActiveRecord::Migration
  def change
    create_table :rides do |t|
      t.string :start_address, :null => false
      t.string :destination_address, :null => false
      t.decimal :price, :precision => 8, :scale => 2, :null => false
      t.float :distance, :precision => 9, :scale => 3, :null => false
      t.date :date, :null => false

      t.timestamps :null => false
    end
  end
end
