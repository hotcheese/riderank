class AddIndexToTaxis < ActiveRecord::Migration
  def change
    add_index :taxis, :name, unique: true
  end
end
