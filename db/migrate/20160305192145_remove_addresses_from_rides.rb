class RemoveAddressesFromRides < ActiveRecord::Migration
  def change
    remove_column :rides, :start_address, :string
    remove_column :rides, :destination_address, :string
  end
end
