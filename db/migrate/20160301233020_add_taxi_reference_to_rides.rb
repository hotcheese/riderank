class AddTaxiReferenceToRides < ActiveRecord::Migration
  def change
    add_belongs_to :rides, :taxi
  end
end
