# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160305192145) do

  create_table "qwes", force: :cascade do |t|
    t.integer  "start_address_id"
    t.string   "destination_address"
    t.decimal  "price"
    t.float    "distance"
    t.date     "date"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "qwes", ["start_address_id"], name: "index_qwes_on_start_address_id"

  create_table "rides", force: :cascade do |t|
    t.decimal  "price",      precision: 8, scale: 2, null: false
    t.float    "distance",                           null: false
    t.date     "date",                               null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "taxi_id"
  end

  create_table "taxis", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "taxis", ["name"], name: "index_taxis_on_name", unique: true

end
